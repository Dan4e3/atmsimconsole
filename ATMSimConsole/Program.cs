﻿using ATMSimConsole.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATMSimConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string executingPath = AppDomain.CurrentDomain.BaseDirectory;
            string fileName = "ATM.txt";

            ATM atm = new ATM(Path.Combine(executingPath, fileName));

            while (!atm.OutOfMoney)
            {
                atm.WaitForMoneyRequest();
                atm.WithdrawMoney();
                atm.PrintLeftATMBalance();
            }
                
        }
    }
}
