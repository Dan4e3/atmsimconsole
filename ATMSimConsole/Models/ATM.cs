﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATMSimConsole.Models
{
    public class ATM
    {
        SortedDictionary<uint, uint> BankNoteBalance { get; set; }
        int TotalBalance { get; set; }
        uint RequestedMoney { get; set; }
        string PathToBalanceFile { get; set; }
        string BanknoteSeparator { get; set; }
        delegate void TotalBalanceChange(int balanceChange);
        event TotalBalanceChange BankNoteBalanceChange;

        public bool OutOfMoney { get; set; }

        public ATM(string pathToFileWithBalance, string bankNoteSep = ":")
        {
            PathToBalanceFile = pathToFileWithBalance;
            BanknoteSeparator = bankNoteSep;
            BankNoteBalance = new SortedDictionary<uint, uint>(new DescendingComparer<uint>());
            BankNoteBalanceChange += ATM_BankNoteBalanceChange;
            ParseBalanceFile();
        }

        private void ATM_BankNoteBalanceChange(int balanceChange)
        {
            TotalBalance += balanceChange;
        }

        private void ParseBalanceFile()
        {
            string[] fileContents = File.ReadAllLines(PathToBalanceFile);
            foreach (string line in fileContents)
            {
                string[] splitter = line.Split(new string[] { BanknoteSeparator }, StringSplitOptions.None);
                KeyValuePair<uint, uint> pairToAdd = new KeyValuePair<uint, uint>(Convert.ToUInt16(splitter[0]), Convert.ToUInt16(splitter[1]));
                BankNoteBalance.Add(pairToAdd.Key, pairToAdd.Value);
                BankNoteBalanceChange?.Invoke((int)pairToAdd.Key * (int)pairToAdd.Value);
            }
        }

        public void WaitForMoneyRequest()
        {
            uint requestedAmount;
            Console.Write("Enter amount of money to withdraw: ");
            try
            {
                requestedAmount = Convert.ToUInt16(Console.ReadLine());
            }
            catch
            {
                Console.WriteLine("The number entered is incorrect!");
                return;
            }

            if (requestedAmount > TotalBalance)
            {
                Console.WriteLine("ATM doesn't have enough money to withdraw!");
                return;
            }

            if (requestedAmount == 0)
            {
                Console.WriteLine("Impossible to withdraw 0!");
                return;
            }

            RequestedMoney = requestedAmount;
        }

        public void WithdrawMoney()
        {
            if (RequestedMoney <= 0)
                return;

            uint requestedAmountLeft = RequestedMoney;
            SortedDictionary<uint, uint> withdrawMoneyTable = new SortedDictionary<uint, uint>(new DescendingComparer<uint>());

            for (int i = 0; i < BankNoteBalance.Count; i++)
            {
                KeyValuePair<uint, uint> bankNotePair = BankNoteBalance.ElementAt(i);

                if (bankNotePair.Value == 0 || bankNotePair.Key > RequestedMoney)
                    continue;

                if (requestedAmountLeft == 0)
                    break;

                GetBanknotesForMoneyRequest(ref requestedAmountLeft, bankNotePair, withdrawMoneyTable);
            }

            if (requestedAmountLeft > 0)
            {
                Console.WriteLine("ATM doesn't have fitting banknotes for you to withdraw!");
                return;
            }
            else
            {
                PrintOutBalanceResult("Withdrawal result:", withdrawMoneyTable);
                CommitWithdrawal(withdrawMoneyTable);
            }
        }

        private void PrintOutBalanceResult(string message, SortedDictionary<uint, uint> withdrawingTable)
        {
            Console.WriteLine();
            Console.WriteLine(message);
            foreach (var pair in withdrawingTable)
                Console.WriteLine($"{pair.Key}:{pair.Value}");
        }

        private void CommitWithdrawal(SortedDictionary<uint, uint> withdrawingTable)
        {
            foreach (var pair in withdrawingTable)
            {
                BankNoteBalance[pair.Key] -= pair.Value;
                BankNoteBalanceChange?.Invoke((int)pair.Key * (int)pair.Value);
            }
        }

        public void PrintLeftATMBalance()
        {
            Console.WriteLine();
            Console.WriteLine("ATM's left banknotes (testing purposes only!)");
            Console.WriteLine("-----");
            foreach (var pair in BankNoteBalance)
                Console.WriteLine($"| {pair.Key}:{pair.Value} |");
            Console.WriteLine("-----");
        }

        private void GetBanknotesForMoneyRequest(ref uint leftAmountRequested, KeyValuePair<uint, uint> bankNotePair,
            SortedDictionary<uint, uint> withdrawingTable)
        {
            uint bankNoteCount = 0;
            uint bankNoteValue = bankNotePair.Key;
            for (int i = 0; ; i++)
            {
                if (leftAmountRequested < bankNotePair.Key || 
                    leftAmountRequested == 0 ||
                    BankNoteBalance[bankNotePair.Key] == 0)
                {
                    if (bankNoteCount != 0)
                    {
                        withdrawingTable.Add(bankNoteValue, bankNoteCount);
                        BankNoteBalance[bankNotePair.Key] += bankNoteCount;
                    }
                    return;
                }

                leftAmountRequested -= bankNotePair.Key;
                BankNoteBalance[bankNotePair.Key] -= 1;
                bankNoteCount += 1;
            }
        }

        class DescendingComparer<T> : IComparer<T> where T : IComparable<T>
        {
            public int Compare(T x, T y)
            {
                return y.CompareTo(x);
            }
        }
    }
}
